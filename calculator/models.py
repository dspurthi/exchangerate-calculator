from django.db import models

# Create your models here.
class Dailyrate(models.Model):
    rate_date = models.CharField(max_length=200,unique=True)
    er_USD = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    er_CNY = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    er_GBP = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    er_INR = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    er_NZD = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    er_CAD = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)