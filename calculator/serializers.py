from rest_framework import serializers
from .models import Dailyrate

class rateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dailyrate
        fields = '__all__'