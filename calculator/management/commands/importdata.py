from django.core.management.base import BaseCommand, CommandError
from calculator.models import Dailyrate
import csv

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str)

    def handle(self, *args, **options):
        print(options["filename"])
        with open(options["filename"], newline='') as f:
            reader = csv.reader(f)
            data = list(reader)
            for line in data:
                req_data = {}
                req_data['rate_date'] = line[0]
                req_data['er_USD'] = line[1]
                req_data['er_CNY'] = line[2]
                req_data['er_GBP'] = line[3]
                req_data['er_INR']= line[4]
                req_data['er_NZD']= line[5]
                req_data['er_CAD']= line[6]
            # print(req_data)
                Dailyrate.objects.create(**req_data)