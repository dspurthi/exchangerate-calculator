from django.urls import path
from . import views

urlpatterns = [
    path('allexchangerates/', views.rates_list),
    path('calculator/',views.index)

]