from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse,JsonResponse, request
from rest_framework import serializers
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt

from .models import Dailyrate
from .serializers import rateSerializer

def index(request):
    latest_exchangerate_list = Dailyrate.objects.order_by('er_INR')[:5]
    template = loader.get_template('calculator/index.html')
    context = {
        'latest_exchangerate_list': latest_exchangerate_list,
    }
    return HttpResponse(template.render(context, request))

# def index(request):
#     latest_question_list = Dailyrate.objects.order_by('-rate_date')[:5]
#     context = {'latest_exchangerate_list': latest_exchangerate_list}
#     return render(request, 'calculator/index.html',context)

@csrf_exempt
def rates_list(request):
    if request.method == 'GET':
        rates_list = Dailyrate.objects.all()
        serializer = rateSerializer(rates_list,many=True)
        return JsonResponse(serializer.data,safe= False)


    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = rateSerializer(data=data,many=True)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201,safe=False)
        return JsonResponse(serializer.errors,status=400)
    elif request.method == 'DELETE':
        rates_list = Dailyrate.objects.all()
        rates_list.delete()
        return HttpResponse("successful !!!!")
